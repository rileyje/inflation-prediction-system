#Senior Design Dataset

dat<-read.csv("DatasetV5.csv")
#Normalize function and conversion
normalize <- function(x) {(x - min(x, na.rm=TRUE))/(max(x,na.rm=TRUE) - min(x, na.rm=TRUE))}
Norm_GE = normalize(dat[11]);Norm_GE
Norm_CPI = normalize(dat[2]);Norm_CPI


#########################################################################################################
#Time Series Analysis
#########################################################################################################
class(dat)
Norm_GE<-ts(Norm_GE, start=c(1960,1), end=c(2018,1), frequency = 4)
Norm_GE
Norm_GE=na.omit(Norm_GE)
Norm_CPI<-ts(Norm_CPI, start=c(1960,1), end=c(2018,1), frequency=4)
Norm_CPI
Norm_CPI=na.omit(Norm_CPI)
time(Norm_GE)
plot(Norm_GE)
abline(reg = lm(Norm_GE~time(Norm_GE)))
dev.off()
decompose(Norm_GE)
plot(decompose(Norm_GE))
class(Norm_GE)
class(Norm_CPI)
plot(Norm_CPI,Norm_GE)
ts.plot(Norm_CPI,Norm_GE,col=c("blue","red"))
acf(Norm_GE, na.action = na.pass)
#correlation between 2
acf(ts.intersect(as.matrix(Norm_GE,Norm_CPI, na.action = na.pass)))


#forcasting
plot(HoltWinters(Norm_GE,alpha = 0.5,beta=0.2,gamma =1))
plot(HoltWinters(Norm_CPI,alpha = 1,beta=0.25,gamma =0))

Norm_GE.Holt<-HoltWinters(Norm_GE)
dev.off()
Norm_CPI.Holt<-HoltWinters(Norm_CPI)
Norm_GE.predict<-predict(Norm_GE.Holt, n.ahead=2*4)
class(Norm_GE.predict)
ts.plot(Norm_GE, Norm_GE.predict,lty=1:2)
#rejecting Holtwinters model for predictions, good for forcasting

#Stochastic Models
#randomwalk
acf(diff(Norm_GE))

#stationary autoregressive process


#partial autocorrelation ( autocorraltion that takes previous period into accound)
pacf(Norm_GE)

#autoregression (gives coefficients to lag periods)
#U.ar<-ar(Urate,method = "mle") #mle = maximum likelihood estimation
#acf(U.ar$res[-1],na.action = na.pass)

#regression on time dimension

Norm_GE.reg<-lm(Norm_GE~time(Norm_GE))
par(mfrow=c(2,2))
plot(Norm_GE.reg)
Norm_CPI.reg<-lm(Norm_CPI~time(Norm_CPI))
par(mfrow=c(2,2))
#plot(Norm_CPI.reg)
length(Norm_GE)
Norm_GE
length(Norm_CPI)
#Norm_CPI<-ts(dat$Norm_CPI, start=c(1960,1), end=c(2018,1), frequency = 4)
Norm_CPI

Norm_GE<-ts(Norm_GE, start=c(1960,1), end=c(2018,1), frequency = 4)
Norm_GEVsNorm_CPI<-lm(Norm_CPI~Norm_GE)
par(mfrow=c(2,2))
plot(Norm_GEVsNorm_CPI)
Norm_GEVsNorm_CPITime<-lm(time(Norm_CPI)~time(Norm_GE))
par=(mfrow=c(2,2))
plot(Norm_GEVsNorm_CPITime)
summary(Norm_GE.reg)
acf(resid(Norm_GE.reg))
pacf(resid(Norm_GE.reg))
par(mfrow=c(2,2))
plot(Norm_GE.reg)

is.na(Norm_GE)
Norm_GE<-ts(Norm_GE, start=c(1960,1), end=c(2018,1), frequency = 4)
Norm_CPI<-ts(Norm_CPI, start=c(1960,1), end=c(2018,1), frequency = 4)
time(Norm_GE)
#PR<-na.omit(Norm_GE)
Norm_GE
abline(reg = lm(Norm_GE~time(Norm_GE)))
dev.off()
plot(Norm_GE)
decompose(Norm_GE)
plot(decompose(Norm_GE))
ts.plot(Norm_CPI,Norm_GE,col=c("blue","red"))
acf(Norm_GE, na.action = na.pass)
plot(HoltWinters(Norm_GE,alpha = 0.7,beta= 0.2,gamma = 1))
Norm_GE.reg<-lm(Norm_GE~time(Norm_GE))
par(mfrow=c(2,2))
plot(Norm_GE.reg)
Norm_CPI.reg<-lm(Norm_CPI~time(Norm_CPI))
par(mfrow=c(2,2))
plot(Norm_CPI.reg)

length(Norm_GE)
length(Norm_CPI)
Norm_GEVsNorm_CPI<-lm(Norm_CPI~Norm_GE)
par(mfrow=c(2,2))
plot(Norm_GEVsNorm_CPI)
length(time(Norm_GE))

Norm_GE<-ts(Norm_GE, start=c(1960,1), end=c(2018,1), frequency = 4)
Norm_GEVsNorm_CPITime<-lm(time(Norm_CPI)~time(Norm_GE))
par=(mfrow=c(2,2))
plot(Norm_GEVsNorm_CPITime)
summary(Norm_GE.reg)
acf(resid(Norm_GE.reg))
pacf(resid(Norm_GE.reg))
par(mfrow=c(2,2))
plot(Norm_GE.reg)

Norm_GE<-ts(Norm_GE, start=c(1960,1), end=c(2018,1),frequency = 4)
time(Norm_GE)
Norm_GE<-na.omit(Norm_GE)
Norm_GE
abline(reg = lm(Norm_GE~time(Norm_GE)))
plot(Norm_GE)
decompose(Norm_GE)
plot(decompose(Norm_GE))
ts.plot(Norm_CPI,Norm_GE,col=c("blue","red"))
acf(Norm_GE, na.action = na.pass)
plot(HoltWinters(Norm_GE,alpha = 0.7,beta=0.25,gamma =1))
Norm_GE.reg<-lm(Norm_GE~time(Norm_GE))
par(mfrow=c(2,2))
plot(Norm_GE.reg)
Norm_CPI.reg<-lm(Norm_CPI~time(Norm_CPI))
par(mfrow=c(2,2))
plot(Norm_CPI.reg)

length(Norm_GE)
length(Norm_CPI)

Norm_GE<-ts(Norm_GE, start=c(1960,1), end=c(2018,1),frequency = 4)
Norm_GEVsNorm_CPI<-lm(Norm_CPI~Norm_GE)
par(mfrow=c(2,2))
plot(Norm_GEVsNorm_CPI)

Norm_GEVsNorm_CPITime<-lm(time(Norm_CPI)~time(Norm_GE))
par=(mfrow=c(2,2))
plot(Norm_GEVsNorm_CPITime)
summary(Norm_GE.reg)
acf(resid(Norm_GE.reg))
pacf(resid(Norm_GE.reg))
par(mfrow=c(2,2))
plot(Norm_GE.reg)

#ARIMA (Autoregressive moving average)
# MA -> moving average model ( looks for white noise or random component left after removing trends)
#Ma(3) (account 3 random components)
Norm_GE.ma<-arima(Norm_GE,order=c(0,0,3))
Norm_GE.ma
# 1 prior of Norm_GE,
Norm_GE.ma<-arima(Norm_GE,order=c(1,0,1))
#TODO
Norm_GE.ma
#
Norm_GE.ma<-arima(Norm_GE,order=c(2,1,2))
Norm_GE.ma
Norm_GE.predict<-predict(Norm_GE.ma, n.ahead = 60)
ts.plot(Norm_GE,Norm_GE.predict$pred, lty=1:2)
#bad model

#GARCH
#Generalized autoregressive conditioned heteroskedastic
#install.packages('tseries')
#replace NA's with linear estimates
#install.packages("dplyr")
library(dplyr)



Norm_GE<-ts(Norm_GE, start=c(1960,1), end=c(2018,1), frequency = 4)


length(Norm_GE)
#dat_NAsreplaced <- dat %>% filter(!is.na(Norm_GE))
length(Norm_CPI)
Norm_GE<-ts(Norm_GE, start=c(1960,1), end=c(2010,1), frequency = 4)
Norm_CPI<-ts(Norm_CPI, start=c(1960,1), end=c(2010,1), frequency = 4)

Norm_GE=na.omit(Norm_GE)
Norm_GE
Norm_CPI
length(Norm_GE)
#dat_NAsreplaced <- dat %>% filter(!is.na(Norm_GE))
length(Norm_CPI)
#fit <- lm(Norm_CPI ~ Norm_GE, data = dat)
#Norm_GE
#dat_NAsreplaced$Norm_GE

library(tseries)
length(Norm_GE)
Norm_GE <- na.omit(Norm_GE)
Norm_GE.garch<-garch(Norm_GE,grad="numerical")
par(mfrow=c(5,5))
plot(Norm_GE.garch)
decompose(Norm_GE)


########################
#Descision tree
###########################
library(rpart)
dat2<-read.csv("DatasetV5.csv")

regressor<-rpart(formula = dat2$Year ~ . , data=dat2)
regressor

y_pred = predict(regressor)
y_pred
library(ggplot2)
dev.off()

ggplot() + 
  geom_point(aes(x=Norm_GE,y=dat2$�..Year),
             colour='red') + 
  geom_line(aes(x=Norm_GE,y=predict(regressor,newdata = dat2)),
            colour='blue') +
  ggtitle("Decision TreeRegression") + 
  xlab('Government Expenditures') +
  ylab
####
#plot2
x_grid=seq(min(dat2$�..Norm_CPI),max(dat2$�..Norm_CPI),0.1)
ggplot() + 
  geom_point(aes(x=Norm_GE,y=dat2$�..Norm_CPI),
             colour='red') + 
  geom_line(aes(x=dat2$Norm_GE,y=predict(regressor,newdata = dat2)),
            colour='blue') +
  ggtitle("Decision TreeRegression") + 
  xlab('Government Expenditures') +
  ylab('CPI')

#################################################################
# Random Forest
#################################################################
library(ggplot2)
library(randomForest)
set.seed(123)
regressor1<-randomForest(x=dat2[4],
                         y=dat2$Minimum.Wage.Rates,
                         ntree=10)

y_pred2 = predict(regressor1)

x_grid=seq(min(dat2$UnemploymentRate),max(dat2$Minimum.Wage.Rates),0.1)
ggplot() + 
  geom_point(aes(x=dat2$UnemploymentRate,y=dat2$Minimum.Wage.Rates),
             colour='red') + 
  geom_line(aes(x=dat2$UnemploymentRate,y=predict(regressor1,newdata = dat2)),
            colour='blue') +
  ggtitle("Random Forestn") + 
  xlab('Minimum Wage') +
  ylab('Norm_CPI')
