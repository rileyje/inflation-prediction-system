#Senior Design Dataset

dat<-read.csv("DatasetV5.csv")
#Normalize function and conversion
normalize <- function(x) {(x - min(x, na.rm=TRUE))/(max(x,na.rm=TRUE) - min(x, na.rm=TRUE))}
Norm_GDI = normalize(dat[10]);Norm_GDI
Norm_Inflation = normalize(dat[5]);Norm_Inflation



#########################################################################################################
#Time Series Analysis
#########################################################################################################
class(dat)
Norm_GDI<-ts(Norm_GDI, start=c(1960,1), end=c(2018,1), frequency = 4)
Norm_GDI
Norm_GDI=na.omit(Norm_GDI)
Norm_Inflation<-ts(Norm_Inflation, start=c(1960,1), end=c(2018,1), frequency=4)
Norm_Inflation
Norm_Inflation=na.omit(Norm_Inflation)
time(Norm_GDI)
plot(Norm_GDI)
abline(reg = lm(Norm_GDI~time(Norm_GDI)))
dev.off()
decompose(Norm_GDI)
plot(decompose(Norm_GDI))
class(Norm_GDI)
class(Norm_Inflation)
plot(Norm_Inflation,Norm_GDI)
ts.plot(Norm_Inflation,Norm_GDI,col=c("blue","red"))
acf(Norm_GDI, na.action = na.pass)
#correlation between 2
acf(ts.intersect(as.matrix(Norm_GDI,Norm_Inflation, na.action = na.pass)))


#forcasting
plot(HoltWinters(Norm_GDI,alpha = 0.5,beta=0.2,gamma =1))
plot(HoltWinters(Norm_Inflation,alpha = 1,beta=0.25,gamma =0))

Norm_GDI.Holt<-HoltWinters(Norm_GDI)
dev.off()
Norm_Inflation.Holt<-HoltWinters(Norm_Inflation)
Norm_GDI.predict<-predict(Norm_GDI.Holt, n.ahead=2*4)
class(Norm_GDI.predict)
ts.plot(Norm_GDI, Norm_GDI.predict,lty=1:2)
#rejecting Holtwinters model for predictions, good for forcasting

#Stochastic Models
#randomwalk
acf(diff(Norm_GDI))

#stationary autoregressive process


#partial autocorrelation ( autocorraltion that takes previous period into accound)
pacf(Norm_GDI)

#autoregression (gives coefficients to lag periods)
#U.ar<-ar(Urate,method = "mle") #mle = maximum likelihood estimation
#acf(U.ar$res[-1],na.action = na.pass)

#regression on time dimension

Norm_GDI.reg<-lm(Norm_GDI~time(Norm_GDI))
par(mfrow=c(2,2))
plot(Norm_GDI.reg)
Norm_Inflation.reg<-lm(Norm_Inflation~time(Norm_Inflation))
par(mfrow=c(2,2))
#plot(Norm_Inflation.reg)
length(Norm_GDI)
Norm_GDI
length(Norm_Inflation)
#Norm_Inflation<-ts(dat$Norm_Inflation, start=c(1960,1), end=c(2018,1), frequency = 4)
Norm_Inflation

Norm_GDI<-ts(Norm_GDI, start=c(1960,1), end=c(2018,1), frequency = 4)
Norm_GDIVsNorm_Inflation<-lm(Norm_Inflation~Norm_GDI)
par(mfrow=c(2,2))
plot(Norm_GDIVsNorm_Inflation)
Norm_GDIVsNorm_InflationTime<-lm(time(Norm_Inflation)~time(Norm_GDI))
par=(mfrow=c(2,2))
plot(Norm_GDIVsNorm_InflationTime)
summary(Norm_GDI.reg)
acf(resid(Norm_GDI.reg))
pacf(resid(Norm_GDI.reg))
par(mfrow=c(2,2))
plot(Norm_GDI.reg)

is.na(Norm_GDI)
Norm_GDI<-ts(Norm_GDI, start=c(1960,1), end=c(2018,1), frequency = 4)
Norm_Inflation<-ts(Norm_Inflation, start=c(1960,1), end=c(2018,1), frequency = 4)
time(Norm_GDI)
#PR<-na.omit(Norm_GDI)
Norm_GDI
abline(reg = lm(Norm_GDI~time(Norm_GDI)))
dev.off()
plot(Norm_GDI)
decompose(Norm_GDI)
plot(decompose(Norm_GDI))
ts.plot(Norm_Inflation,Norm_GDI,col=c("blue","red"))
acf(Norm_GDI, na.action = na.pass)
plot(HoltWinters(Norm_GDI,alpha = 0.7,beta= 0.2,gamma = 1))
Norm_GDI <- na.omit(Norm_GDI)
Norm_GDI.reg<-lm(Norm_GDI~time(Norm_GDI))
par(mfrow=c(2,2))
plot(Norm_GDI.reg)
Norm_Inflation.reg<-lm(Norm_Inflation~time(Norm_Inflation))
par(mfrow=c(2,2))
plot(Norm_Inflation.reg)

length(Norm_GDI)
length(Norm_Inflation)
Norm_GDI<-ts(Norm_GDI, start=c(1960,1), end=c(2018,1), frequency = 4)
Norm_GDI
Norm_Inflation
Norm_GDIVsNorm_Inflation<-lm(Norm_Inflation~Norm_GDI)
par(mfrow=c(2,2))
plot(Norm_GDIVsNorm_Inflation)
length(time(Norm_GDI))

Norm_GDI<-ts(Norm_GDI, start=c(1960,1), end=c(2018,1), frequency = 4)

Norm_GDIVsNorm_InflationTime<-lm(time(Norm_Inflation)~time(Norm_GDI))
par=(mfrow=c(2,2))
plot(Norm_GDIVsNorm_InflationTime)
summary(Norm_GDI.reg)
acf(resid(Norm_GDI.reg))
pacf(resid(Norm_GDI.reg))
par(mfrow=c(2,2))
plot(Norm_GDI.reg)

Norm_GDI<-ts(Norm_GDI, start=c(1960,1), end=c(2018,1),frequency = 4)
time(Norm_GDI)
Norm_GDI<-na.omit(Norm_GDI)
Norm_GDI
abline(reg = lm(Norm_GDI~time(Norm_GDI)))
plot(Norm_GDI)
decompose(Norm_GDI)
plot(decompose(Norm_GDI))
ts.plot(Norm_Inflation,Norm_GDI,col=c("blue","red"))
acf(Norm_GDI, na.action = na.pass)
plot(HoltWinters(Norm_GDI,alpha = 0.7,beta=0.25,gamma =1))
Norm_GDI.reg<-lm(Norm_GDI~time(Norm_GDI))
par(mfrow=c(2,2))
plot(Norm_GDI.reg)
Norm_Inflation.reg<-lm(Norm_Inflation~time(Norm_Inflation))
par(mfrow=c(2,2))
plot(Norm_Inflation.reg)

length(Norm_GDI)
length(Norm_Inflation)

Norm_GDI<-ts(Norm_GDI, start=c(1960,1), end=c(2018,1),frequency = 4)
Norm_GDIVsNorm_Inflation<-lm(Norm_Inflation~Norm_GDI)
par(mfrow=c(2,2))
plot(Norm_GDIVsNorm_Inflation)

Norm_GDIVsNorm_InflationTime<-lm(time(Norm_Inflation)~time(Norm_GDI))
par=(mfrow=c(2,2))
plot(Norm_GDIVsNorm_InflationTime)
summary(Norm_GDI.reg)
acf(resid(Norm_GDI.reg))
pacf(resid(Norm_GDI.reg))
par(mfrow=c(2,2))
plot(Norm_GDI.reg)

#ARIMA (Autoregressive moving average)
# MA -> moving average model ( looks for white noise or random component left after removing trends)
#Ma(3) (account 3 random components)
Norm_GDI.ma<-arima(Norm_GDI,order=c(0,0,3))
Norm_GDI.ma
# 1 prior of Norm_GDI,
Norm_GDI.ma<-arima(Norm_GDI,order=c(1,0,1))
#TODO
Norm_GDI.ma
#
Norm_GDI.ma<-arima(Norm_GDI,order=c(2,1,2))
Norm_GDI.ma
Norm_GDI.predict<-predict(Norm_GDI.ma, n.ahead = 60)
ts.plot(Norm_GDI,Norm_GDI.predict$pred, lty=1:2)
#bad model

#GARCH
#Generalized autoregressive conditioned heteroskedastic
#install.packages('tseries')
#replace NA's with linear estimates
#install.packages("dplyr")
library(dplyr)



Norm_GDI<-ts(Norm_GDI, start=c(1960,1), end=c(2018,1), frequency = 4)


length(Norm_GDI)
#dat_NAsreplaced <- dat %>% filter(!is.na(Norm_GDI))
length(Norm_Inflation)
Norm_GDI<-ts(Norm_GDI, start=c(1960,1), end=c(2010,1), frequency = 4)
Norm_Inflation<-ts(Norm_Inflation, start=c(1960,1), end=c(2010,1), frequency = 4)

Norm_GDI=na.omit(Norm_GDI)
Norm_GDI
Norm_Inflation
length(Norm_GDI)
#dat_NAsreplaced <- dat %>% filter(!is.na(Norm_GDI))
length(Norm_Inflation)
#fit <- lm(Norm_Inflation ~ Norm_GDI, data = dat)
#Norm_GDI
#dat_NAsreplaced$Norm_GDI

library(tseries)
length(Norm_GDI)
Norm_GDI <- na.omit(Norm_GDI)
Norm_GDI.garch<-garch(Norm_GDI,grad="numerical")
par(mfrow=c(5,5))
plot(Norm_GDI.garch)
dev.off()
decompose(Norm_GDI)


########################
#Descision tree
###########################
library(rpart)
dat2<-read.csv("DatasetV5.csv")

regressor<-rpart(formula = dat2$Year ~ . , data=dat2)
regressor

y_pred = predict(regressor)
y_pred
library(ggplot2)
dev.off()

ggplot() + 
  geom_point(aes(x=Norm_GDI,y=dat2$�..Year),
             colour='red') + 
  geom_line(aes(x=Norm_GDI,y=predict(regressor,newdata = dat2)),
            colour='blue') +
  ggtitle("Decision TreeRegression") + 
  xlab('Gross Domestic Income') +
  ylab
####
#plot2
x_grid=seq(min(dat2$�..Norm_Inflation),max(dat2$�..Norm_Inflation),0.1)
ggplot() + 
  geom_point(aes(x=Norm_GDI,y=dat2$�..Norm_Inflation),
             colour='red') + 
  geom_line(aes(x=dat2$Norm_GDI,y=predict(regressor,newdata = dat2)),
            colour='blue') +
  ggtitle("Decision TreeRegression") + 
  xlab('Gross Domestic Income') +
  ylab('Inflation')

#################################################################
# Random Forest
#################################################################
library(ggplot2)
library(randomForest)
set.seed(123)
regressor1<-randomForest(x=dat2[4],
                         y=dat2$Minimum.Wage.Rates,
                         ntree=10)

y_pred2 = predict(regressor1)

x_grid=seq(min(dat2$UnemploymentRate),max(dat2$Minimum.Wage.Rates),0.1)
ggplot() + 
  geom_point(aes(x=dat2$UnemploymentRate,y=dat2$Minimum.Wage.Rates),
             colour='red') + 
  geom_line(aes(x=dat2$UnemploymentRate,y=predict(regressor1,newdata = dat2)),
            colour='blue') +
  ggtitle("Random Forestn") + 
  xlab('Minimum Wage') +
  ylab('Norm_Inflation')
